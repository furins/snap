$.getScript("data.js", function(){
    window.buildings = buildings;
});

function distance(lat1, lon1, lat2, lon2) {
    var R = 6371;
    var a =
        0.5 - Math.cos((lat2 - lat1) * Math.PI / 180) / 2 +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        (1 - Math.cos((lon2 - lon1) * Math.PI / 180)) / 2;
    console.log(lat1, lon1, lat2, lon2);
    return R * 2 * Math.asin(Math.sqrt(a));
}
//
// function geocode(place, callback) {
//     var url = 'http://open.mapquestapi.com/search?format=json&q=' + encodeURIComponent(place);
//     // requires jquery for ajax
//     $.getJSON(url, function(data) {
//         if (data.length > 0) {
//             callback(null, {
//                 lon: parseFloat(data[0].lon),
//                 lat: parseFloat(data[0].lat)
//             })
//         } else {
//             callback(null, null)
//         }
//     });
// }

function format_address(locality) {
    function addInfo(value, prefix, suffix) {
        prefix = prefix || '';
        suffix = suffix || '';
        if (typeof(value) !== 'undefined') {
            return prefix + value + suffix;
        } else {
            return '';
        }
    }
    if (locality.geocodeQuality === 'COUNTY') {
        return 'County/Province of '+locality.adminArea4+' ('+addInfo(locality.adminArea3)+addInfo(locality.adminArea2, ', ')+addInfo(locality.adminArea1, ', ')+')';
    } else if (locality.geocodeQuality === 'CITY')  {
        return 'City of '+locality.adminArea5+addInfo(locality.adminArea4, ' (', ')')+' '+addInfo(locality.adminArea3)+addInfo(locality.adminArea2, ', ')+addInfo(locality.adminArea1, ', ');
    } else if (locality.geocodeQuality === 'STREET') {
        return 'Street '+locality.street+addInfo(locality.adminArea5, ' ')+addInfo(locality.adminArea4, ' (', ')')+' '+addInfo(locality.adminArea3)+addInfo(locality.adminArea2, ', ')+addInfo(locality.adminArea1, ', ');
    } else {
        return addInfo(locality.street)+addInfo(locality.adminArea5, ' ')+addInfo(locality.adminArea4, ' (', ')')+' '+addInfo(locality.adminArea3)+addInfo(locality.adminArea2, ', ')+addInfo(locality.adminArea1, ', ');
    }
}

var simulator = {};

$(document).ready(function() {
    simulator.map = L.map('map').setView([48, 11], 4);
    var layer = L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpeg', {
        attribution: 'Tiles by <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        subdomains: '1234'
    });
    layer.addTo(simulator.map);
    simulator.layergroup = assetLayerGroup = new L.LayerGroup();
    simulator.layergroup.addTo(simulator.map);
    $('.ui.dropdown').dropdown();

});


$("#study_area").change(function() {
    var geojsonMarkerOptions = {
        radius: 3,
        fillColor: "#ff7800",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };

    var features_url = $(this).val();
    if (features_url !== '') {
        $.ajax({
            url: features_url,
            success: function(data) {
                simulator.layergroup.clearLayers();
                simulator.features = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        return L.circleMarker(latlng, geojsonMarkerOptions);
                    }
                });
                simulator.map.fitBounds(simulator.features.getBounds());
                simulator.layergroup.addLayer(simulator.features);
            },
            dataType: 'json'
        });
    }
});


$('#calculate').click(function() {
    var url = "http://open.mapquestapi.com/geocoding/v1/address?key=Fmjtd%7Cluu8216ynd%2C7s%3Do5-942sg0&location="+encodeURIComponent($('#locality').val())+"&callback=?";
    var tons = parseInt($('#buildings'));

    $.getJSON(url).done( function( response ) {
        $('#results').empty();
        var text = '';
        if (response.results[0].locations.length){
            text += '<h4 class="ui header">Localities found:</h4><div class="ui celled list">';
            for (index = 0; index < response.results[0].locations.length; ++index) {
                var name = format_address(response.results[0].locations[index]);
                text += '<div class="item"><div class="content"><div class="header">'+name+'<div class="right floated compact ui primary button" onclick="simulator.map.setView('+JSON.stringify(response.results[0].locations[index].latLng).replace(/"/g, '&quot;')+', 12); selectLocality({latLon:'+JSON.stringify(response.results[0].locations[index].latLng).replace(/"/g, '&quot;')+', name:\''+name+'\'});">Build here <i class="chevron right icon"></i></div><div class="right floated compact ui button" onclick="simulator.map.setView('+JSON.stringify(response.results[0].locations[index].latLng).replace(/"/g, '&quot;')+', 12);">Show map</div></div></div></div>';
                console.log(response.results[0].locations[index]);
            }
            text += '</div>';

    } else {
        text += 'No localities found that match your destination area.';
    }
    $('#results').append(text);
    });
});


function getBuildingById(id) {
    id = parseInt(id);
    for (index = 0; index < window.buildings.length; ++index) {
        if (window.buildings[index].id === id) {
            return window.buildings[index]
        }
    }
    return {'name':'unknown building', 'id':null};
}

function selectLocality(data) {
    $('#results').empty();
    var building = getBuildingById($('#buildings').val());
    var tons = parseInt(building.tons);
    var amount_of_sand = tons/3;
    var amount_of_gravels = amount_of_sand * 2;
    var volume_of_sand = Math.round(amount_of_sand / 1.4); // cubic meters
    var volume_of_gravels = Math.round(amount_of_gravels / 1.92); // cubic meters
    var CO2_emitted = tons * 0.000090; // euro 5 truck
    var CO2_emitted_train = tons * 0.000063; // euro 5 truck
    var possible_recycled_amount_sand = Math.round(volume_of_sand * 0.3);
    var possible_recycled_amount_gravels = Math.round(volume_of_gravels * 0.3);
    var number_of_trucks = Math.round((volume_of_sand + volume_of_gravels) / 20);

    var closest_quarry_distance = 0;

    $.getJSON("data/sites.json", function(json) {
        console.log(json);
        var optimal_site = null;
        var optimal_distance = 9999999999999999;
        for (index = 0; index < json.features.length; ++index) {
            var new_distance = distance(parseFloat(data.latLon.lat), parseFloat(data.latLon.lng), parseFloat(json.features[index].geometry.coordinates[1]), parseFloat(json.features[index].geometry.coordinates[0]));
            console.log(parseFloat(json.features[index].geometry.coordinates[1]));
            console.log(new_distance);
            if (new_distance <= optimal_distance) {
                optimal_distance = Math.round(new_distance);
                optimal_site = json.features[index];
            }
        }


        var text = '<h4 class="ui header">Build '+building.long_name+' in '+data.name+'</h4>';
        text += '<div class="ui divider"></div>';
        text += '<p>The average amount of aggregates required to build '+building.long_name+' is '+building.tons+' tons.</p>';
        text += '<p>The amount of sand required is approximately '+Math.round(amount_of_sand)+' tons ('+volume_of_sand+'m<sup>3</sup>), and '+Math.round(amount_of_gravels)+' tons of gravels ('+volume_of_gravels+'m<sup>3</sup>).</p>';
        text += '<p>To transport the material to the building site '+number_of_trucks+' trips from the quarry/recycling facilities are required. The total CO<sub>2</sub> emissions will be '+Math.round(CO2_emitted)+' tons/km (by an EURO 5 engine).</p>';
        text += '<p>The closest quarry ('+optimal_site.properties.NOME+') is '+optimal_distance+' Km far from the building site, thus the minimum CO<sub>2</sub> emissions will be equal to '+Math.round(optimal_distance*CO2_emitted)+' tons ('+(Math.round(optimal_distance*100/35))+'% better than an average source, like a too far quarry/recycling facility); You can additionally save '+Math.round(optimal_distance*(CO2_emitted-CO2_emitted_train))+' tons of CO<sub>2</sub> by using train transportation, where possible.</p>';
        text += '<p>Using recycled aggregates, without compromising the quality of concrete, you can save '+(possible_recycled_amount_sand+possible_recycled_amount_gravels)+'m<sup>3</sup> of rock/sand to be excavated from a nearby quarry.</p>';

        $('#results').append(text);
    });


}
