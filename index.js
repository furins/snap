// usual requirements
var express = require('express'),
    cookieParser = require('cookie-parser'),
    i18n = require('i18n'),
    nconf = require('nconf'),
    path    = require("path"),
    moment = require("moment"),
    app = module.exports = express();


// load settings from environmental variables or config file

nconf.env('__') // please note that any env variable will override config settings stored in config.json. You can use namespace__keyname notation.
    .file({
        file: 'config.json'
    });


// load locales


i18n.configure({
    // setup some locales - other locales default to en silently
    locales: nconf.get('locale:languages'),

    // sets a custom cookie name to parse locale settings from
    cookie: nconf.get('server_conf:cookie'),

    // where to store json files - defaults to './locales'
    directory: __dirname + '/locales'
});

// you will need to use cookieParser to expose cookies to req.cookies
app.use(cookieParser());

// i18n init parses req for language headers, cookies, etc.
app.use(i18n.init);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');



app.use("/templates", express.static(__dirname + '/templates'));
app.use("/vendors", express.static(__dirname + '/bower_components'));
app.use("/img", express.static(__dirname + '/img'));
app.use("/js", express.static(__dirname + '/js'));
app.use("/css", express.static(__dirname + '/css'));
app.use("/data", express.static(__dirname + '/data'));


// serving homepage
app.get('/', function(req, res) {
    var today = moment().format('dddd, DD MMMM YYYY');
    res.render('er/index', { title: 'HOME', date: today});
})

.get('/simulator', function(req, res) {
    var today = moment().format('dddd, DD MMMM YYYY');
    res.render('simulator', { title: 'SNAP-SEE simulator',
    date: today,
    databases: nconf.get('databases'),
    buildings: nconf.get('buildings')});
})

.get('/data.js', function(req, res) {
    res.send('buildings='+JSON.stringify(nconf.get('buildings'))+';');
})

// serving custom pages, these are not related to the simulator and may be removed completely
app.get('/intro', function(req, res) {
    var today = moment().format('dddd, DD MMMM YYYY');
    res.render('er/intro', { title: 'Costruzioni sostenibili', date: today});
})

.get('/presentazione', function(req, res) {
    var today = moment().format('dddd, DD MMMM YYYY');
    res.render('er/presentazione', { title: 'Aggregati sostenibili', date: today});
})

.get('/aggregati', function(req, res) {
    var today = moment().format('dddd, DD MMMM YYYY');
    res.render('er/aggregati', { title: 'Quanti aggregati servono?', date: today});
})

.get('/sostenibili', function(req, res) {
    var today = moment().format('dddd, DD MMMM YYYY');
    res.render('er/sostenibili', { title: 'Aggregati sostenibili?', date: today});
})
;




// starting server
if (!module.parent) {
    app.listen(nconf.get('PORT'));
}
