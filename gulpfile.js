var gulp = require('gulp')
  , nodemon = require('gulp-nodemon');


gulp.task('dev', function () {
  nodemon({ script: 'index.js', ext: 'html js', ignore: ['ignored.js'] })
    // .on('change', ['lint'])
    .on('restart', function () {
      console.log('restarted!')
    })
})
