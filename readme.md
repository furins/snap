SNAP-SEE Best practices simulator
=================================


*version 1.0.0*
**developed by Emilia-Romagna Region as a tool to support WP6 approaches**

Prerequisites
-------------

 - Node js
 - npm


Install
-------

    npm install


Run
---

    node index.js


Configuration
-------------

to configurate the app, please modify `config.json` and the files in the `locales` folder
